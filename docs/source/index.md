% Lumache documentation master file, created by
% sphinx-quickstart on Thu Oct 28 12:45:32 2021.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

```{include} ../../README.md
:relative-images:
```

For further info, check out {doc}`usage`.
Read installation instructions in {ref}`Installation`.

```{warning}
This library is under heavy development.
```

```{toctree}
:caption: 'Contents:'
:maxdepth: 2

usage
```
